<?php
  require_once 'lib/library.php';
  require_once 'lib/connect-db.php';
  require_once 'lib/mail.php';

  $review_msg = <<<EOD
Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Nullam dictum euismod egestas.
Suspendisse turpis justo, tincidunt sit amet quam vel, tristique tempus quam.
Vivamus nec risus mattis."
EOD;

  try {
    if (isset($_GET['add'])) {
      if (empty($_GET['fname']))
        throw new Exception("Set firstname");

      if (empty($_GET['lname']))
        throw new Exception("Set lastname");

      if (empty($_GET['email']))
        throw new Exception("Set e-mail");

      if (empty($_GET['company']))
        throw new Exception("Set company name");

      $firstname = trim($_GET['fname']);
      $lastname = trim($_GET['lname']);
      $email = trim($_GET['email']);
      $company = trim($_GET['company']);

      if (!filter_var($email, FILTER_VALIDATE_EMAIL))
        throw new Exception("Set correct e-mail");

      Review::add($firstname, $lastname, $email, $company);

      $mail = file_get_contents("mails/feedback.eml");
      $mail = strtr($mail, array(
        '{FROM}'     => "admin@".$_SERVER['HTTP_HOST'],
        '{NAME}'     => $firstname." ".$lastname,
        '{TO}'       => $email,
        '{SITE}'     => $_SERVER['HTTP_HOST'],
        '{CONTENT}'  => $review_msg
      ));
      $mail = mailenc($mail);

      if (!mailx($mail, true))
        throw new Exception("Mail send error.");

      $success = "Review added";
    }
  } catch (PDOException $e) {
    $error = $e->getMessage();
  } catch (Exception $e) {
    $error = $e->getMessage();
  }

  if (isset($success)):
?>
    <p class="text-center text-success"><?= $success ?></p>
<?php
  endif;

  if (isset($error)):
?>
    <p class="text-center text-danger"><?= $error ?></p>
<?php
  endif;
  
  $reviews = Review::get_all();

  foreach ($reviews as $review) {
?>
    <div class="review">
      <blockquote>
        <p class="text-center"><?= $review_msg ?></p>
      </blockquote>

      <p class="text-center"><strong><?= $review['firstname']." ".$review['lastname'] ?></strong><br>
        <?= $review['email'] ?> at <?= $review['company'] ?></p>
    </div>
<?php
  }
?>