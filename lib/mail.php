<?php
  function mailx($mail, $body_encode = false) {
    list($head, $body) = preg_split("/\r?\n\r?\n/s", $mail, 2);
    $to = "";
    if (preg_match("/^To:\s*([^\r\n]*)[\r\n]*/m", $head, $p)) {
      $to = @$p[1];
      $head = str_replace($p[0], "", $head);
    }
    $subject = "";
    if (preg_match("/^Subject:\s*([^\r\n]*)[\r\n]*/m", $head, $p)) {
      $subject = @$p[1];
      $head = str_replace($p[0], "", $head);
    }
    $head .= "\r\nDate: ".date(DATE_RFC822);
    if ($body_encode) {
      $head .= "\r\nContent-Transfer-Encoding: base64";
      $body = base64_encode($body);
    }
    return mail($to, $subject, $body, trim($head));
  }

  function mailenc($mail) {
    list ($head, $body) = preg_split("/\r?\n\r?\n/s", $mail, 2);
    $encoding = "";
    if (preg_match("/^Content-type:\s*\S+\s*;\s*charset\s*=\s*(\S+)/mi", $head, $p))
      $encoding = $p[1];
    $newhead = "";
    foreach (preg_split("/\r?\n/s", $head) as $line) {
      $line = mailenc_header($line, $encoding);
      $newhead .= "$line\r\n";
    }
    return "$newhead\r\n$body";
  }

  function mailenc_header($header, $encoding = "UTF-8") {
    return preg_replace_callback(
      "/([\x7F-\xFF][^<>\r\n]*)/s",
      function ($p) use ($encoding) {
        preg_match('/^(.*?)(\s*)$/s', $p[1], $sp);
        return "=?$encoding?B?".base64_encode($sp[1])."?=".$sp[2];
      },
      $header
    );
  }
?>