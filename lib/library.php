<?php
  abstract class DBC {
		public static $con;

		public static function connect($host, $dbname, $user, $password) {
      $dsn = "mysql:host=".$host.";dbname=".$dbname;

      self::$con = new PDO($dsn, $user, $password, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
      self::$con->query("SET NAMES utf8");
		}
  }

  class Review {
    public static function add($firstname, $lastname, $email, $company) {
      $query = "INSERT INTO feedback VALUES (NULL, '{$firstname}', '{$lastname}', '{$email}', '{$company}', NOW())";
      $result = DBC::$con->query($query);
    }

    public static function get_all() {
      $query = "SELECT * FROM feedback ORDER BY id DESC";
      $result = DBC::$con->query($query);

      return $result->fetchAll(PDO::FETCH_ASSOC);
    }
  }
?>