<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CSG Invotas</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/main.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <header>
      <div class="container-fluid">
        <div class="row">
          <div class="container">
            <div class="col-md-6 hidden-sm hidden-xs">
              <img src="images/mac.png" alt="iMac">
            </div>
            <div class="col-md-offset-1 col-md-5">
              <h1>What is Automated Threat Response?</h1>

              <p>Automated Threat Response solves your biggest problem when cyber attacks hammer your network—response time.
                By integrating your tools, workflows and processes, you can detect and contain threats in just seconds.</p>
            </div>
          </div>
        </div>
      </div>
    </header>

    <div class="container-fluid page-block" id="block-2">
      <div class="row">
        <div class="container">
          <div class="row">
            <div class="col-md-7" id="why-choose">
              <h3>Why Choose Automated Threat Response  from CSG Invotas?</h3>
              
              <p>Security Orchestrator fits in all enterprises and is hardware and operating system agnostic.
                Architecturally, security orchestrator takes feeds from SIEMs and threat intelligence solutions as triggers
                to start digital response workflows that pulls vital system data for context and directs actions to your security tools.
                Federated options are available.</p>

              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-6"><p class="item"><i class="fa fa-check fa-2x"></i> <strong>Complements</strong> every step of the Automated Threat Response framework</p></div>
                  <div class="col-md-6"><p class="item"><i class="fa fa-check fa-2x"></i> <strong>Digitizes</strong> your cyber playbook based on runbooks and approved processes</p></div>
                  <div class="clearfix"></div>

                  <div class="col-md-6 item"><p class="item"><i class="fa fa-check fa-2x"></i> <strong>Integrates</strong> your existing security infrastructure</p></div>
                  <div class="col-md-6 item"><p class="item"><i class="fa fa-check fa-2x"></i> <strong>Synchronizes</strong> large-scale changes with analyst oversight using workflows</p></div>
                  <div class="clearfix"></div>

                  <div class="col-md-6 item"><p class="item"><i class="fa fa-check fa-2x"></i> <strong>Unifies</strong> security tools and data feeds for actionable information</p></div>
                  <div class="col-md-6 item"><p class="item"><i class="fa fa-check fa-2x"></i> <strong>Offers</strong> a low-risk, cost effective deployment</p></div>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>
            
            <div class="col-md-5" id="free-download">
              <form>
                <img src="images/paper.png" alt="">
                <fieldset>
                  <legend>Free Download</legend>
                  <p><em>Speed is Everything: Reducing Exposure to Cyber Attacks</em></p>
                  <div class="clearfix"></div>
                  <hr>

                  <p>Fill out the form below to get your free copy of this comprehensive guide to automated threat response technology.</p>
                  <div class="form-group">
                    <label for="firstname">First Name *</label>
                    <input type="text" name="firstname" class="form-control" id="firstname">
                  </div>
                  <div class="form-group">
                    <label for="lastname">Last Name *</label>
                    <input type="text" name="lastname" class="form-control" id="lastname">
                  </div>
                  <div class="form-group">
                    <label for="email">Email Address *</label>
                    <input type="text" name="email" class="form-control" id="email">
                  </div>
                  <div class="form-group">
                    <label for="company">Company Name *</label>
                    <input type="text" name="company" class="form-control" id="company">
                  </div>
 
                  <button type="button" onclick="reviewAdd()" class="btn">Download Now</button>
                </fieldset>
              </form>
            </div>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div id="feedback">
<?php
  require('feedback.php');
?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid page-block" id="block-3">
      <div class="row">
        <div class="container">
          <div class="row">
            <div class="col-md-12" id="steps">
              <h3 class="text-center">An Automated Threat Response Framework Uses These Steps</h3>
            </div>
            <div class="clearfix"></div>

            <div class="col-md-6">
              <img src="images/icon-1.png" alt="Icon">
              <h4>Unify Your Defenses</h4>
              <p>Save time and resources when attacked by unifying incident-related data and your security technologies under a single management platform.
                Connect your technologies to integrate your defenses.</p>
            </div>
            <div class="col-md-6">
              <img src="images/icon-3.png" alt="Icon">
              <h4>Automate Your Win</h4>
              <p>Keep your analysts focused on the mission by reducing the need for manual tasks.
                By automating small actions that can be integrated with other manual or automated actions in a workflow, you stay in control and increase accuracy.</p>
            </div>
            <div class="clearfix"></div>

            <div class="col-md-6">
              <img src="images/icon-2.png" alt="Icon">
              <h4>Orchestrate Your Response</h4>
              <p>You can move beyond paper compliance to a fully digital workflow that provides both control and flexibility.
                Execute digital response plans by fusing people, processes and technologies into approved, measurable workflows.</p>
            </div>
            <div class="col-md-6">
              <img src="images/icon-4.png" alt="Icon">
              <h4>Act With Confidence</h4>
              <p>Security orchestration with automation helps you greatly reduce the time it takes to contain and mitigate security incidents.
                Realize the full potential of your security investments at machine speed.</p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <footer>
      <div class="container-fluid">
        <div class="row">
          <div class="container">
            <div class="col-md-12">
              <h3 class="text-center">Select CSG International Clients</h3>
              <div class="clearfix"></div>

              <p class="text-center">
                <a href="#" class="client"><img src="images/clients/client-1.png" alt="Client Logo"></a>
                <a href="#" class="client"><img src="images/clients/client-2.png" alt="Client Logo"></a>
                <a href="#" class="client"><img src="images/clients/client-3.png" alt="Client Logo"></a>
                <a href="#" class="client"><img src="images/clients/client-4.png" alt="Client Logo"></a>
                <a href="#" class="client"><img src="images/clients/client-5.png" alt="Client Logo"></a>
                <a href="#" class="client"><img src="images/clients/client-6.png" alt="Client Logo"></a>
                <a href="#" class="client"><img src="images/clients/client-7.png" alt="Client Logo"></a>
              </p>

              <hr>
              
              <p class="text-center">&copy; 2014 CSG Systems International, Inc. and/or its affiliates (&ldquo;CSG&rdquo;). All rights reserved. Privacy Policy</p>
            </div>
          </div>
        </div>
      </div>
    </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

    <script src='js/all.js'></script>
  </body>
</html>