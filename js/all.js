function createRequestObject() {
  if (typeof XMLHttpRequest === 'undefined') {
    XMLHttpRequest = function() {
      try {
        return new ActiveXObject("Msxml2.XMLHTTP.6.0");
      } catch(e) {}
      
      try {
        return new ActiveXObject("Msxml2.XMLHTTP.3.0");
      } catch(e) {}
      
      try {
        return new ActiveXObject("Msxml2.XMLHTTP");
      } catch(e) {}
      
      try {
        return new ActiveXObject("Microsoft.XMLHTTP");
      } catch(e) {}
      
      throw new Error("This browser does not support XMLHttpRequest.");
    };
  }
  
  return new XMLHttpRequest();
}

function reviewAdd() {
  var uri = "feedback.php";
  var getval = "?fname=" + encodeURIComponent(document.getElementById("firstname").value) +
               "&lname=" + encodeURIComponent(document.getElementById("lastname").value) +
               "&email=" + encodeURIComponent(document.getElementById("email").value) +
               "&company=" + encodeURIComponent(document.getElementById("company").value) + 
               "&add";

  uri += getval;

  xmlhttp = createRequestObject();
  xmlhttp.open("GET", uri, true);
  xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xmlhttp.onreadystatechange = function (e) {
    if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200)) {
      document.getElementById("feedback").innerHTML = xmlhttp.responseText;
      if (document.getElementById("feedback").getElementsByClassName("text-success").length) {
        resetForm();
      }
    }
  };
  xmlhttp.send(null);
}

function resetForm() {
  document.getElementById("firstname").value = "";
  document.getElementById("lastname").value = "";
  document.getElementById("email").value = "";
  document.getElementById("company").value = "";
}